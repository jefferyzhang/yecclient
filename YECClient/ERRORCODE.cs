﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YECClient
{
    class ERRORCODE
    {
        public const int BaseCode = 1000;
        public const int ERROR_SUCCESS = 0;
        public const int ERROR_OPENEVENT = BaseCode+1;
        public const int ERROR_FILENOEXIST = BaseCode + 2;
        public const int ERROR_CONNECT = BaseCode + 3;
        public const int ERROR_ABORT = BaseCode + 4;
        public const int ERROR_FAIL = BaseCode + 5;
    }
}
