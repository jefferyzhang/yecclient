﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace YECClient
{
    class Client : IDisposable
    {
        public class ReceivedEventArgs : EventArgs
        {
            internal string _data;

            public ReceivedEventArgs(string data)
            {
                this._data = data;
            }

            public string Data
            {
                get
                {
                    return this._data;
                }
            }
        }
       

        public static EventWaitHandle ShutdownEvent = new EventWaitHandle(false, EventResetMode.ManualReset);
        public static ConcurrentQueue<String> listitem = new ConcurrentQueue<String>();
        private sealed class Receiver
        {
            internal event EventHandler<ReceivedEventArgs> DataReceived;

            internal Receiver(NetworkStream stream)
            {
                _stream = stream;
                ShutdownEvent.Reset();
                _thread = new Thread(Run);
                _thread.Start();
            }

            private void Run()
            {
                try
                {
                    // ShutdownEvent is a ManualResetEvent signaled by
                    // Client when its time to close the socket.
                    while (!ShutdownEvent.WaitOne(0))
                    {
                        try
                        {
                            byte[] _data = new byte[1000];
                            int nlen = 0;
                            // We could use the ReadTimeout property and let Read()
                            // block.  However, if no data is received prior to the
                            // timeout period expiring, an IOException occurs.
                            // While this can be handled, it leads to problems when
                            // debugging if we are wanting to break when exceptions
                            // are thrown (unless we explicitly ignore IOException,
                            // which I always forget to do).
                            if (!_stream.DataAvailable)
                            {
                                // Give up the remaining time slice.
                                Thread.Sleep(5);
                            }
                            else if ((nlen = _stream.Read(_data, 0, _data.Length)) > 0)
                            {
                                // Raise the DataReceived event w/ data...
                                String s = System.Text.Encoding.Default.GetString(_data, 0, nlen);
                                ReceivedEventArgs e = new ReceivedEventArgs(s);
            
                                if (DataReceived!=null) DataReceived(_stream,e);
                                listitem.Enqueue(s);
                            }
                            else
                            {
                                // The connection has closed gracefully, so stop the
                                // thread.
                                ShutdownEvent.Set();
                            }
                        }
                        catch (IOException ex)
                        {
                            // Handle the exception...
                            Program.LogIt(ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception...
                    Program.LogIt(ex.ToString());
                }
                finally
                {
                    _stream.Close();
                }
            }

            private NetworkStream _stream;
            private Thread _thread;
        }
        public void SendData(byte[] data)
        {
            _stream.Write(data, 0, data.Length);
            _stream.Flush();
            _stream.Write(new byte[] { 0x0d }, 0, 1);
            _stream.Flush();
            String s = System.Text.Encoding.Default.GetString(data, 0, data.Length);
            Program.LogIt("==>"+s);
        }

        public void SendByte(byte[] data)
        {
            _stream.Write(data, 0, data.Length);
            _stream.Flush();
            String s = System.Text.Encoding.Default.GetString(data, 0, data.Length);
            Program.LogIt("==>" + s);
        }

        // Consumers register to receive data.
        public event EventHandler<ReceivedEventArgs> DataReceived;

        public Boolean IsConnectServer()
        {
            return _client != null && _client.Connected;
        }
        public Client(String ip , int nPort)
        {
            try
            {
                _client = new TcpClient(ip, nPort);
                _stream = _client.GetStream();
                _receiver = new Receiver(_stream);
                _receiver.DataReceived += OnDataReceived;
            }
            catch (Exception e)
            {
                Program.LogIt(e.ToString());
            }

        }

        private void OnDataReceived(object sender, ReceivedEventArgs e)
        {
            var handler = DataReceived;
            if (handler != null) DataReceived(this, e);  // re-raise event
        }

        private TcpClient _client;
        private NetworkStream _stream;
        private Receiver _receiver;

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            ShutdownEvent.Set();
            _client.Close();   
        }
    }
}
