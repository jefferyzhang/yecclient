﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace YECClient
{
    class Program
    {
        static String g_sFlag ="";
        public static void LogIt(String s)
        {
            Console.Write(g_sFlag+"   :");
            Console.WriteLine(s);
        }
        private static EventWaitHandle _eventDotask;
            // new EventWaitHandle(false, EventResetMode.AutoReset, @"TheName");
        private static EventWaitHandle _eventStop;// = new ManualResetEvent(false);
        private static AutoResetEvent _eventWaitClean = new AutoResetEvent(true);
        private static AutoResetEvent _eventWaitRecv = new AutoResetEvent(false);

        static void Usage()
        {
            Console.WriteLine("-ip=");
            Console.WriteLine("-port=49152/49153");
            Console.WriteLine("-file= task script file");
            Console.WriteLine("-y     do task sclient");
            Console.WriteLine("-info  read HDD Info");
            Console.WriteLine("-start do task for another process.");
            Console.WriteLine("-exit exit task for another process.");

        }

        static int Main(string[] args)
        {
            LogIt(String.Format("{0} start: ++ version: {1}",
                Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName),
                Process.GetCurrentProcess().MainModule.FileVersionInfo.FileVersion));

            InstallContext _param = new InstallContext(null, args);
            String sIP = "192.168.0.1";
            int sPort = 0xC000;
            String sFile = "";
            if(_param.IsParameterTrue("help"))
            {
                Usage();
                return ERRORCODE.ERROR_SUCCESS;
            }
            if(_param.Parameters.ContainsKey("ip"))
            {
                sIP = _param.Parameters["ip"];
            }
            if (_param.Parameters.ContainsKey("port"))
            {
                int.TryParse(_param.Parameters["port"], out sPort);
            }
            g_sFlag = sIP + ":" + sPort;

            if (_param.Parameters.ContainsKey("file"))
            {
                sFile = Environment.ExpandEnvironmentVariables(_param.Parameters["file"]);
            }

            if(_param.IsParameterTrue("exit"))
            {
                try
                {
                    _eventStop = EventWaitHandle.OpenExisting(string.Format(@"Global\{0}_{1}_exit", sIP.Replace(".","_"), sPort));
                    if(_eventStop!=null)
                        _eventStop.Set();

                    return ERRORCODE.ERROR_SUCCESS;
                }catch(Exception e)
                {
                    LogIt(e.ToString());
                    return ERRORCODE.ERROR_OPENEVENT;
                }
            }
            else if (_param.IsParameterTrue("start"))
            {
                try
                {
                    _eventDotask = EventWaitHandle.OpenExisting(string.Format(@"Global\{0}_{1}_start", sIP.Replace(".", "_"), sPort));
                    if (_eventDotask != null)
                        _eventDotask.Set();
                    return ERRORCODE.ERROR_SUCCESS;
                }
                catch (Exception e)
                {
                    LogIt(e.ToString());
                    return ERRORCODE.ERROR_OPENEVENT;
                }
            }
            _eventStop = new EventWaitHandle(false, EventResetMode.AutoReset, string.Format(@"Global\{0}_{1}_exit", sIP.Replace(".", "_"), sPort));
            _eventDotask = new EventWaitHandle(false, EventResetMode.AutoReset, string.Format(@"Global\{0}_{1}_start", sIP.Replace(".", "_"), sPort));

            LogIt("Config file is " + sFile);
            if (!File.Exists(sFile) && !_param.IsParameterTrue("info"))
            {
                Console.WriteLine("give file, please, -file=**");
                return ERRORCODE.ERROR_FILENOEXIST;
            }

            String FileNameWo = Path.GetFileNameWithoutExtension(sFile);


            const int CMD_DELAY = 1000;

            Client client = new Client(sIP, sPort);
            int nRetry = 10;
            while(!client.IsConnectServer()&&nRetry-->0)
            {
                Thread.Sleep(1000);
                client = new Client(sIP, sPort);
            }
            if (!client.IsConnectServer()) return ERRORCODE.ERROR_CONNECT;
            client.DataReceived += client_DataReceived;
            ClearData();

            client.SendData(new byte[] { 0x08 });//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();
            client.SendData(new byte[0] {  });//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();

            client.SendData(System.Text.Encoding.Default.GetBytes(@"set_prompt Z\r\n"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            ClearData();
            
            client.SendData(System.Text.Encoding.Default.GetBytes("printvar"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr();
            String sInfo = GetSBString();
            ClearData();
            if(_param.IsParameterTrue("info"))
            {
                FileNameWo = "SA_Read_Info";
            }

            client.SendData(System.Text.Encoding.Default.GetBytes("script_load num=1 \"name=SwitchChkm\""));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            ClearData();
            client.SendData(new byte[]{0x40, 0x65, 0x63, 0x68, 0x6f, 0x20, 0x22, 0x5c, 0x5c, 0x21, 0x22});//@echo "\\!"\r
            _eventWaitRecv.WaitOne(CMD_DELAY);
            ClearData();
            client.SendByte(new byte[] { 0x04 });//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();

            client.SendData(System.Text.Encoding.Default.GetBytes(string.Format("script_load num=3 \"name={0}\"", FileNameWo)));
            _eventWaitRecv.WaitOne(CMD_DELAY);
            ClearData();
            String[] items;
            if(_param.IsParameterTrue("info")){
                items = Regex.Split(YECClient.Properties.Resources.SA_Read_Info, "\r\n");
            }
            else{
                items = File.ReadAllLines(sFile);
            }
            foreach(var s in items)
            {
                ClearData();
                client.SendData(System.Text.Encoding.Default.GetBytes(s));
                client.SendData(new byte[] { 0x0d });
                _eventWaitRecv.WaitOne(CMD_DELAY);
            }
            ClearData();
            client.SendByte(new byte[] { 0x04 });//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();

            client.SendData(System.Text.Encoding.Default.GetBytes("menu_set category=online num=1\r"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();

            client.SendData(System.Text.Encoding.Default.GetBytes("@set power.on_interval 2000"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();

            client.SendData(System.Text.Encoding.Default.GetBytes("led_control num=3 color=0 blink=0\r"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();

            _eventStop.Reset();
            _eventDotask.Reset();
            if (_param.IsParameterTrue("y") || _param.IsParameterTrue("info")) _eventDotask.Set();
            int nExit = WaitHandle.WaitAny(new WaitHandle[] { _eventStop, _eventDotask });
            if (nExit == 0)
            {
                Console.WriteLine("exit event recieve.");
                client.Close();
                return ERRORCODE.ERROR_SUCCESS;
            }
            else if (nExit == 1)
            {
                Console.WriteLine("do task event recieve.");
                client.SendData(System.Text.Encoding.Default.GetBytes("script_run num=3"));//\r\nZ\r\n
                _eventWaitRecv.WaitOne(CMD_DELAY);
            }
            bool bWorking = true;
            int nCount = 300;
            int nRet = ERRORCODE.ERROR_SUCCESS;
            while (bWorking)
            {
                if (_eventStop.WaitOne(0))
                {
                    client.SendData(new byte[] { 0x03, 0x0D });//\r\nZ\r\n
                    _eventWaitRecv.WaitOne(CMD_DELAY);
                    CheckLastStr("Z\r\n",500); ClearData();
                    bWorking = false;
                }
                if (_eventWaitClean.WaitOne(1000))
                {
                    string s = GetSBString(); 
                    if(s.IndexOf(">PASS\r\n")>=0)
                    {
                        ClearData();
                        nRet = ERRORCODE.ERROR_SUCCESS;
                        bWorking = false;
                    }
                    else if (s.IndexOf(">FAIL\r\n") >= 0)
                    {
                        ClearData();
                        nRet = ERRORCODE.ERROR_FAIL;
                        bWorking = false;
                    }
                    else if (s.IndexOf(">ABORT\r\n") >= 0)
                    {
                        ClearData();
                        nRet = ERRORCODE.ERROR_ABORT;
                        bWorking = false;
                    }
                    else if(s.EndsWith("\r\n"))
                    {
                        ClearData();
                    }
                    nCount = 300;
                }
                else
                {
                    if (nCount-- < 0) bWorking = false;
                }
            }
            if(_param.IsParameterTrue("info"))
            {
                client.SendData(System.Text.Encoding.Default.GetBytes("printvar"));//\r\nZ\r\n
                _eventWaitRecv.WaitOne(CMD_DELAY);
                CheckLastStr();
                //String sInfo = GetSBString();
                ClearData();
            }
            client.SendData(System.Text.Encoding.Default.GetBytes("menu_set category=online num=1\r"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();
            client.SendData(System.Text.Encoding.Default.GetBytes("led_control num=3 color=1 blink=0\r"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();
            client.SendData(System.Text.Encoding.Default.GetBytes("led_control num=3 color=0 blink=0\r"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();
            client.SendData(System.Text.Encoding.Default.GetBytes("led_control num=1 color=0 blink=0\r"));//\r\nZ\r\n
            _eventWaitRecv.WaitOne(CMD_DELAY);
            CheckLastStr(); ClearData();

            client.Close();

            return nRet;
        }
        //static Boolean bclear = false;
        static StringBuilder sb = new StringBuilder();
        static Boolean CheckLastStr(string send="Z\r\n", int nTryCnt = 50, int interval=100)
        {
            Boolean b = GetSBString().EndsWith(send);
            while(!b && nTryCnt-- > 0)
            {
                Thread.Sleep(interval);
                b = GetSBString().EndsWith(send);
            }
            return b;
        }
        static String GetSBString()
        {
            String s="";
            lock (sb)
            {
                s = sb.ToString();
            }
            return s;
        }
        static void ClearData()
        {
            lock (sb)
            {
                LogIt("<=="+sb.ToString());
                sb.Clear();
            }
        }
        static void client_DataReceived(object sender, Client.ReceivedEventArgs e)
        {
            lock (sb)
            {
                sb.Append(e.Data);
            }
            if (e.Data.EndsWith("\r\n"))
            {
                _eventWaitRecv.Set();
            }

            _eventWaitClean.Set();
        }

    }
}
